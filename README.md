# Event Streaming Help #

This project is meant as a tool to be used by organizations trying to live-stream/broadcast events virtually.

# How To Get Started #

1. Ensure you have the complete project, a full version should be available at https://gitlab.com/WilliamBehrens/streamhelp ).

2. Now that you have the full project you can either double click the home.html file or drap and drop it into a browser to view it.

That should be enough to get you started
